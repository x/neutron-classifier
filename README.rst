===============================
neutron-classifier
===============================

This project provides a common API plugin for services to classify traffic in
Neutron as well as an agent extension implementation to distribute these classifications.

The neutron-classifier provides a common API for all neutron services to deliver
a consistent interface for tenants to define specific traffic classifications.
The usage of the agent extension aids in the propagation of classifications to the
backend. The project rose from the need to create a plugin that is common to all
projects in order to prevent duplicates or varied versions of traffic classification work.
The currently supported classification types include ethernet, ipv4, IPv6, IPv4, tcp, udp.
In order to group those types and provide a common relation, classification groups are used.

Any feedback from users is welcome. The features as well as the API may evolve
based on that feedback.

* Free software: Apache license
* Documentation: https://specs.openstack.org/openstack/neutron-specs/specs/pike/common-classification-framework.html
* Wiki: https://wiki.openstack.org/wiki/Neutron/CommonClassificationFramework
* Source: https://opendev.org/x/neutron-classifier/
* Bugs: https://bugs.launchpad.net/neutron

Features
--------
* Flexible classification of network traffic


Background on the Subject of Common Classification Frameworks
-------------------------------------------------------------
* Original Neutron RFE: https://bugs.launchpad.net/neutron/+bug/1476527
* Traffic Classification in Neutron QoS: https://bugs.launchpad.net/neutron/+bug/1527671
